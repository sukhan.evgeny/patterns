package sukhan.mycompany.decorator;

public class Filling implements Order {
    protected Order order;
    protected String name;
    protected Double price;

    public Filling(Order order, double price, String name) {
        this.order = order;
        this.name = name;
        this.price = price;
    }

    @Override
    public Double getPrice() {
        return order.getPrice() + price;
    }
    @Override
    public String getName() {
        return order.getName() + " вместе с " + name;
    }
}
