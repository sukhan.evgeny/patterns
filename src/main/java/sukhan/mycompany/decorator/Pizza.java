package sukhan.mycompany.decorator;

public class Pizza implements Order {

    private String name;
    private Double price;

    public Pizza(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public Double getPrice() {
        return price;
    }
    @Override
    public String getName() {
        return name;
    }
}
