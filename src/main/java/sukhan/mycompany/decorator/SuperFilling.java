package sukhan.mycompany.decorator;

public class SuperFilling extends Filling {
    public SuperFilling(Order order, double price, String name) {
        super(order, price, name);
    }
    public Double getPrice() {
        return this.price * 2 + this.order.getPrice();
    }
    @Override
    public String getName() {
        return order.getName() + " вместе с Супер Добавкой " + this.name;
    }
}
