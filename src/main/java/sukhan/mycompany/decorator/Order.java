package sukhan.mycompany.decorator;

public interface Order {
    public Double getPrice();
    public String getName();
}
