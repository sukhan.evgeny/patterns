package sukhan.mycompany;

import sukhan.mycompany.adapter.*;
import sukhan.mycompany.decorator.Filling;
import sukhan.mycompany.decorator.Order;
import sukhan.mycompany.decorator.Pizza;
import sukhan.mycompany.decorator.SuperFilling;
import sukhan.mycompany.factory.*;
import sukhan.mycompany.singleton.ThreadOne;
import sukhan.mycompany.singleton.ThreadTwo;

/**
 * Классические паттерны
 */
public class App 
{
    public static void main( String[] args ) {
        runAdapter();
        runFactory();
        runDecorator();
        runSingleton();
    }

    private static void runAdapter() {
        Ride ride = new Ride(60);
        Car car = new Car(80);
        Pedestrian pedestrian = new Pedestrian(1, 5);
        System.out.println(ride.itsOK(car));
        System.out.println(ride.itsOK(new PedestrianAdapter(pedestrian)));
    }
    private static void runFactory() {
        FactoryLogic factoryLogic;
        GUIFactory factory;
        TransportType transportType = TransportType.BICYCLE;
        switch (transportType) {
            case CAR:
                factoryLogic = new FactoryLogic(new CarFactory());
                break;
            case BICYCLE:
                factoryLogic = new FactoryLogic(new BicycleFactory());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + transportType);
        }
        factoryLogic.run();
    }
    private static void runDecorator() {
        Order cheese = new Pizza("Cheese Pizza", 10);
        System.out.println(cheese.getName() + ": " + cheese.getPrice() + "$");
        cheese = new Filling(cheese, 4, "Kolbasa");
        System.out.println(cheese.getName() + ": " + cheese.getPrice() + "$");
        cheese = new SuperFilling(cheese,2, "Ananas");
        System.out.println(cheese.getName() + ": " + cheese.getPrice() + "$");
    }
    private static void runSingleton() {
        Thread threadOne = new Thread(new ThreadOne());
        Thread threadTwo = new Thread(new ThreadTwo());
        threadOne.start();
        threadTwo.start();
    }
}
