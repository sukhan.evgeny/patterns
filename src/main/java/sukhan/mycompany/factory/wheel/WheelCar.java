package sukhan.mycompany.factory.wheel;

public class WheelCar implements Wheel {
    @Override
    public void ride() {
        System.out.println("Приводится в движении двигателем");
    }
}
