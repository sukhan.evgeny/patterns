package sukhan.mycompany.factory;

import sukhan.mycompany.factory.rudder.Rudder;
import sukhan.mycompany.factory.wheel.Wheel;

public interface GUIFactory {
    Rudder createRudder();
    Wheel createWheel();
}
