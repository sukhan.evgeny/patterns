package sukhan.mycompany.factory.rudder;

public class RudderCar implements Rudder {
    @Override
    public void twist() {
        System.out.println("Поворачивай руль авто");
    }
}
