package sukhan.mycompany.factory.rudder;

public interface Rudder {
    void twist();
}
