package sukhan.mycompany.factory;

import sukhan.mycompany.factory.rudder.Rudder;
import sukhan.mycompany.factory.rudder.RudderBicycle;
import sukhan.mycompany.factory.wheel.Wheel;
import sukhan.mycompany.factory.wheel.WheelBicycle;

public class BicycleFactory implements GUIFactory {
    @Override
    public Rudder createRudder() {
        return new RudderBicycle();
    }

    @Override
    public Wheel createWheel() {
        return new WheelBicycle();
    }
}
