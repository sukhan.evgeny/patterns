package sukhan.mycompany.factory;

import sukhan.mycompany.factory.rudder.Rudder;
import sukhan.mycompany.factory.wheel.Wheel;

public class FactoryLogic {
    private Rudder rudder;
    private Wheel wheel;

    public FactoryLogic(GUIFactory factory) {
        rudder = factory.createRudder();
        wheel = factory.createWheel();
    }

    public void run() {
        rudder.twist();
        wheel.ride();
    }
}
