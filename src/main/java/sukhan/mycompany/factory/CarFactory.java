package sukhan.mycompany.factory;

import sukhan.mycompany.factory.rudder.Rudder;
import sukhan.mycompany.factory.rudder.RudderCar;
import sukhan.mycompany.factory.wheel.Wheel;
import sukhan.mycompany.factory.wheel.WheelCar;

public class CarFactory implements GUIFactory {

    @Override
    public Rudder createRudder() {
        return new RudderCar();
    }

    @Override
    public Wheel createWheel() {
        return new WheelCar();
    }
}
