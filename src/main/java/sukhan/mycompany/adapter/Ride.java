package sukhan.mycompany.adapter;

public class Ride {
    private double maxSpeed;

    public Ride(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public boolean itsOK(Car car) {
        return this.maxSpeed >= car.getMaxSpeed();
    }

}
