package sukhan.mycompany.adapter;

public class PedestrianAdapter extends Car {
    private Pedestrian pedestrian;

    public PedestrianAdapter(Pedestrian pedestrian) {
        this.pedestrian = pedestrian;
    }

    @Override
    public double getMaxSpeed() {
        return pedestrian.getDistance() / pedestrian.getTimeInHour();
    }

}
