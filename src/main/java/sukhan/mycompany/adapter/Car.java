package sukhan.mycompany.adapter;

public class Car {
    private double maxSpeed;

    public Car() {}

    public Car(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }
}
