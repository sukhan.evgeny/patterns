package sukhan.mycompany.adapter;

public class Pedestrian {
    private double timeInHour;
    private double distance;

    public Pedestrian() {}

    public Pedestrian(double timeInHour, double distance) {
        this.timeInHour = timeInHour;
        this.distance = distance;
    }

    public double getTimeInHour() {
        return timeInHour;
    }

    public double getDistance() {
        return distance;
    }
}
