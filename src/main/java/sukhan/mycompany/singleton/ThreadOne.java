package sukhan.mycompany.singleton;

public class ThreadOne implements Runnable {
    @Override
    public void run() {
        Singleton singleton = Singleton.getInstance("One");
        System.out.println(singleton.value);
    }
}
