package sukhan.mycompany.singleton;

public class ThreadTwo implements Runnable {
    @Override
    public void run() {
        Singleton singleton = Singleton.getInstance("Two");
        System.out.println(singleton.value);
    }
}
